package mis.pruebas.mongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaMongodbApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaMongodbApplication.class, args);
	}

}
