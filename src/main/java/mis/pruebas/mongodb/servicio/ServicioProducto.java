package mis.pruebas.mongodb.servicio;

import mis.pruebas.mongodb.modelo.Producto;
import mis.pruebas.mongodb.modelo.Usuario;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ServicioProducto {

    //POST /productos
    public Producto crearProducto(Producto producto);

    // PUT /productos/{id}
    public Producto reemplazarProducto(String id, Producto productoNuevo);

    //GET /productos
    public List<Producto> obtenerProductos();

    //GET /productos/{id}
    public Producto obtenerProductoPorId(String id);

    //PUT /productos/{id} RequestBody
    public Producto actualizarProducto(Producto producto);

    //DELETE /productos/{id}
    public void borrarProductoPorId(String id);


    //GET /productos/{idProducto}/usuarios
    public List<Usuario> obtenerUsuariosProducto(String idProducto);
}